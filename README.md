### initialize

    terraform init

### preview terraform actions

    terraform plan

### apply configuration with variables

    terraform apply 

### destroy everything fromtf files

    terraform destroy

### show resources and components from current state

    terraform state list

### show current state of a specific resource/data

    terraform state show aws_vpc.eks-vpc    

